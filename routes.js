const express = require('express')
const multer = require('multer')
const upload = multer({ dest: 'files/' })
var fs = require('fs-extra')
var parser = require('./parser')
var db = require('./db')

const port=  process.env.PORT || 8080;
const survey="https://msu.co1.qualtrics.com/jfe/form/SV_3yYECQwJ1IL6hTv"
const save_file = false

const router = express.Router()

router.use(express.static('static'))

router.post('/prefs/:service', upload.single('preferences'), function(req, res, next) {
  console.log("Preferences endpoint")

  type = req.params.service
    info = parser(req.file.path, req.params.service)
    cats = info['categories']
    name = info['name']
    acct_id = info['acct_id']
    db.save_to_db(cats, name, acct_id, req.params.service, req.file.path, 
                  function(user_id, filename) {
      try {
        if (!save_file) {
          console.log("Deleting file " + filename + " for user " + user_id)
          fs.removeSync(filename);
        } else {
          console.log("Renaming file " + filename + " for user " + user_id)
          fs.moveSync(filename, filename + "-" + type + "-" + user_id)
        }
        res.send(user_id)
      } catch(err) {
        console.log("Error with file " + filename + " for user " + user_id)
        if (!save_file) {
          fs.removeSync(filename);
        } else {
          fs.moveSync(filename, filename + "-" + type + "-" + "error-")
        }
        res.status(500).send(err)
      }
    });
});

router.get('/info/:service/num/:user_id', function(req, res) {
  db.num_categories(req.params.user_id, req.params.service, function(num) {
    res.send(num.toString())
  }, 1);
});

router.get('/info/:service/:user_id/:index', function(req, res) {
  db.get_category(req.params.user_id, req.params.service, req.params.index, function(cat) {
    res.send(cat)
  }, 1); 
});

router.get('/info/:service/num/:user_id/enabled', function(req, res) {
  db.num_categories(req.params.user_id, req.params.service, function(num) {
    res.send(num.toString())
  }, 1);
});

router.get('/info/:service/:user_id/enabled/:index', function(req, res) {
  db.get_category(req.params.user_id, req.params.service, req.params.index, function(cat) {
    res.send(cat)
  }, 1); 
});

router.get('/info/:service/num/:user_id/disabled', function(req, res) {
  db.num_categories(req.params.user_id, req.params.service, function(num) {
    res.send(num.toString())
  }, 0);
});

router.get('/info/:service/:user_id/disabled/:index', function(req, res) {
  db.get_category(req.params.user_id, req.params.service, req.params.index, function(cat) {
    res.send(cat)
  }, 0); 
});

router.get('/categories/:service/:user_id', function(req, res) {
  db.get_category_list(req.params.user_id, req.params.service, function(cats) {
    res.send(cats)
  }, 1);
});

module.exports = router

