// Qualtrics.js
// ------------
// These functions are loaded by Qualtrics in the header (the "look and feel section")
// The goal here is to put the majority of javascript code here in this file and
//    only need a single line inside each question to call the code here

var qualtrics_ads_url = "https://futomaki.cas.msu.edu/qualtrics"
//var qualtrics_ads_url = "http://localhost:8080"

function pageInfo(cur_page, per_page) {
	cur_page = parseInt(cur_page)
	per_page = parseInt(per_page)

	var qnum = (cur_page-1) * per_page + 1;
    jQuery(".catnum-first").text(qnum);
    jQuery(".catnum-last").text(qnum+per_page-1);
}

function categoryQuestion(q_on_page, question, cur_page, num_categories, per_page, service, user_id) {
	cur_page = parseInt(cur_page)
	num_categories = parseInt(num_categories)
	
	var qnum = (cur_page-1) * per_page + q_on_page;

	if (qnum > num_categories) {
		jQuery("#"+question.questionId).hide();
		jQuery("#"+question.questionId+"Separator").hide();
	} else {

		jQuery.get(qualtrics_ads_url + "/info/" + service + "/" + user_id + "/"+ qnum, function(data) {
			jQuery('.category'+q_on_page).text(data);
			Qualtrics.SurveyEngine.setEmbeddedData('category_' + question.questionId + '_' + cur_page, data);
		});
   }
}


var num_left_to_return = 2;
function letUserAdvance(question) {
  num_left_to_return = num_left_to_return - 1;
  if (num_left_to_return <= 0) {
  	jQuery('#success').text("Successfully uploaded file")
	question.enableNextButton();
  } else {
 	jQuery('#success').text("Uploading file.........")
  }
}

function getCategoryList(user_id, service, question) {
  jQuery.get(qualtrics_ads_url + "/categories/" + service + "/" + user_id, function(cats) {
  	Qualtrics.SurveyEngine.setEmbeddedData('category_list', cats);
	letUserAdvance(question);
  });
}

function getNumCategories(user_id, per_page, service, question, max_pages) {
	jQuery.get(qualtrics_ads_url + "/info/" + service + "/num/" + user_id, function(numc) {
		Qualtrics.SurveyEngine.setEmbeddedData('num_categories', numc);
        num_pages = Math.ceil(numc / per_page);
        if (num_pages > max_pages) { num_pages = max_pages }
		Qualtrics.SurveyEngine.setEmbeddedData('num_pages', num_pages);
		console.log("Got categories " + numc);
		letUserAdvance(question);
	});
}

function clickToSubmit(question, service, per_page, max_pages) {
    max_pages = parseInt(max_pages);
	question.disableNextButton();
	jQuery('#file-submit').click(function(e) {
  		e.preventDefault();
		jQuery('#errorMessage').hide();
        jQuery('#success').text("Uploading file...")
  		var form = jQuery('#upload-form')[0];
  		var formdata = new FormData(form);
  		jQuery.ajax({
        	type: "POST",
        	enctype: 'multipart/form-data',
			url: qualtrics_ads_url + "/prefs/" + service,
        	data: formdata,
        	processData: false,  // Important!
        	contentType: false,
        	cache: false,
        	success: function(data) {
        		console.log("Successful upload for user " + data + "; getting number of categories")
          		Qualtrics.SurveyEngine.setEmbeddedData('user_id', data);
        		jQuery('#success').text("Uploading file......")
				num_left_to_return = 2;	
				getNumCategories(data, per_page, service, question, max_pages);
				getCategoryList(data, service, question);
			},
			error: function(jqXHR, status, err) {
				jQuery('#success').text('There was a problem uploading your file. ')
				jQuery('#errorMessage').show();
			}
        	});
	});
}


