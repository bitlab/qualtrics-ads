const express = require('express')
const helmet = require('helmet')
const multer = require('multer')
const upload = multer({ dest: 'files/' })
const cors = require('cors')
var parser = require('./parser')
var db = require('./db')

routes = require('./routes')

const port=  process.env.PORT || 8080;
const survey="https://msu.co1.qualtrics.com/jfe/form/SV_3yYECQwJ1IL6hTv"

const app = express()

corsOptions = {
  origin: "https://msu.co1.qualtrics.com"
}

app.use(cors(corsOptions))

app.use(helmet())
app.use(express.static('static'))

app.use('/qualtrics', routes)
app.use('/', routes)

app.listen(port, function() {
  console.log("Listening for requests")
});


