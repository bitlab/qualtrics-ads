module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : 'QualtricsAds',
      script    : 'index.js',
      append_env_to_name: true,
      env: {
		NODE_ENV: 'production',
        DB: 'qualtrics_ads',
        PORT: 4000
      },
      "log_date_format": "YYYY-MM-DD-HH:mm:ss"
    }

  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : 'qualtrics',
      host : 'futomaki.cas.msu.edu',
      ref  : 'origin/master',
      repo : 'git@gitlab.msu.edu:bitlab/qualtrics-ads.git',
      path : '/home/qualtrics/qualtrics-ads-production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
