var fs = require('fs')
var path = require('path')
var he = require('he')

var decode = function(str) {
  return JSON.parse('"' + he.decode(str) + '"')
}

var check_file = function(contents, service) {
  if (service == 'google') {
    regex = /base href="https:\/\/adssettings.google.com\/[^"]*"/
    if (contents.match(regex)) {
      return true
    } else {
      return false
    }
  } else if (service = 'facebook') {
    regex = /<html [^>]*id="facebook" class="[^"]*"/
    if (contents.match(regex)) {
      return true
    } else {
      return false
    }
  } else {
    return false;
  }
}

var parse_name_google = function(contents) {
  try {
    regex = /<div>Google Account<\/div><div class=".....">([^<]+)<\/div><div>([^<]+)<\/div>/
    res = regex.exec(contents) 
    return {name: res[1], acct_id: res[2] }
  } catch(err) {
    return {name: "Error parsing", acct_id: "Error parsing" }
  }
}

var update_category = function(categories, category, id, long_name) {
  categories.forEach(function(val, index, arr) {
    if (val['short_name'] == category) {
      arr[index]['cat_id'] = id;
      arr[index]['long_name'] = long_name;
    }
  });
  return categories;
}

// Parse an ad preferences file from Google
// accepts a string that is the entire file
// returns a javascript object with important information
var parse_google = function(contents) {
  temp = parse_name_google(contents)
  name = temp['name']
  acct_id = temp['acct_id']

  categories = [] // Blank list to start
  id = 1;

  // Start by parsing the HTML section to identify the categories shown to users
  halves = contents.split("What you&#39;ve turned off");

  // Regular expression that matches an array with at least 3 items, the first of which is a number
  // This matches categores that are turned on
  regex = /<div class="c7O9k">([^<]+)<\/div>/gm
  while (res = regex.exec(halves[0])) { // Loop through all matches 
     cat = {cat_id: "", short_name: decode(res[1]), long_name: "", enabled: true, order: id}
     categories.push(cat)
     id = id + 1
  }

  // Grabs the *last* item in the halves array.  Google changes added multiple instances of the split string.....
  while (res = regex.exec(halves[halves.length - 1])) { // Loop through all matches 
     cat = {cat_id: "", short_name: decode(res[1]), long_name: "", enabled: false, order: id}
     categories.push(cat)
     id = id + 1
  }

  // Next, parse the json section to get more information about the categories

  // Regular expression that matches an array with at least 3 items, the first of which is a number
  regex =/\["(\d+)","([^"]+)","([^"]+)",/g;
  while (res = regex.exec(contents)) { // Loop through all matches
     if (res[2].match(/[a-zA-Z]/) && !res[2].match(/\.js$/)) {  // Remove unwanted matches
	   categories = update_category(categories, decode(res[2]), res[1], decode(res[3]));
     }
  }

  return {name: name, acct_id: acct_id, categories: categories};
}

var parse_name_facebook = function(contents) {
  regex = /"NAME":"([^"]+)"/
  res = regex.exec(contents) 
  return res[1]
}

var parse_acct_id_facebook = function(contents) {
  regex = /"ACCOUNT_ID":"([^"]+)"/
  res = regex.exec(contents) 
  return res[1]
}

// Parse an ad preerences file from Facebook
// accepts a string that is the entire file
// returns a javascript object with important information
var parse_facebook = function(contents) {
  name = parse_name_facebook(contents)
  acct_id = parse_acct_id_facebook(contents)

  categories = []
  id = 1
  
  regex = /"fbid":"?([0-9]+)"?,"name":"([^"]+)"/gm
  while (res = regex.exec(contents)) { // Loop through all matches 
    cat = {cat_id: res[1], short_name: decode(res[2]), long_name: "", enabled: true, order: id}
    categories.push(cat);
    id = id + 1
  }

  return {name: name, acct_id: acct_id, categories: categories};
} 

var parse_file = function(filename, type) {
  console.log(`Parsing file ${filename} as ${type}...`)
  var contents = fs.readFileSync(filename, 'utf8');
  if (!check_file(contents, type)) {
    throw "Unrecognized file"
  }

  if (type == "google") {
    return parse_google(contents)
  } else if (type == "facebook") {
    return parse_facebook(contents)
  } else {
    throw "Unknown file type"
  }
}

module.exports = parse_file
