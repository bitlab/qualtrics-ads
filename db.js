var mysql      = require('mysql');
const uuidv4   = require('uuid/v4');
var shuffle    = require('shuffle-array');
var sha1 = require('sha1')

const hash_names = true;

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'rwash',
  password : '**Removed**',
  database : process.env.DB || 'qualtrics_ads'
});

user_table = `
CREATE TABLE users (
  id CHAR(36) NOT NULL PRIMARY KEY,
  name VARCHAR(300),
  account_id VARCHAR(100),
  service VARCHAR(20),
  created_on DATETIME
) ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci`

categories_table = `
CREATE TABLE categories (
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_id CHAR(36) NOT NULL,
  service VARCHAR(10) NOT NULL,
  cat_id VARCHAR(20),
  name VARCHAR(200),
  long_name VARCHAR(500),
  enabled BOOLEAN,
  ordering INTEGER
) ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci`

connection.connect(function(err) {
  if (err) {
	console.error("Error conneting to MySQL database: " + err.stack)
    return;
  }

  console.log("Connected to MySQL database")

  if (require.main == module) {
    // This file was called directly; create needed tables
  
    error_handle = function(error, results, fields) {
      if (error) {
        console.log("Error in Query")
        console.log(error.sqlMessage)
      }
    }

    
    console.log("Creating user table...")
    connection.query("DROP TABLE IF EXISTS users", error_handle)
    connection.query(user_table, error_handle)
    console.log("Creating categories table...")
    connection.query("DROP TABLE IF EXISTS categories", error_handle)
    connection.query(categories_table, error_handle)
    console.log("Done")
    setTimeout(function() {
		process.exit(0)
    }, 3000);
  }
});


var save_user = function(categories, name, acct_id, service, filename, callback) {
  q = "INSERT INTO users (id, name, account_id, service, created_on) VALUES (?, ?, ?, ?, NOW())"
  var id = uuidv4();
  if (hash_names) {
    name = sha1(name)
    acct_id = sha1(acct_id)
  }
  connection.query(q, [id, name, acct_id, service], function(error, results, fields) {
    if (error) {
      console.log("Error saving user: " + error.sqlMessage)
      return
    }
    q = "INSERT INTO categories (user_id, service, cat_id, name, long_name, enabled, ordering) VALUES (?, ?, ?, ?, ?, ?, ?)"
    shuffle(categories).forEach(function(cat) {
      connection.query(q, [id, service, cat.cat_id, cat.short_name, cat.long_name, cat.enabled, cat.order], function(error, results, fields) {
        if (error) {
          console.log(`Error saving category ${cat.short_name}: ` + error.sqlMessage);
          return
        }
      });
    });
    callback(id, filename);
  });
}

// If provided, enabled = 1 or 0 searches for only categories that are enabled or not
// If not provided, returns all categories
var num_categories = function(user_id, service, callback, enabled) {
  if (enabled !== undefined) {
    q = "SELECT count(*) AS num FROM categories WHERE user_id = ? AND service = ? AND enabled = " + enabled
  } else {
    q = "SELECT count(*) AS num FROM categories WHERE user_id = ? AND service = ?"
  }
  connection.query(q, [user_id, service], function(error, results, fields) {
    if (error) {
      console.log("Error counting categories: " + error.sqlMessage);
      callback(0);
      return;
    } 
    result = results[0].num;
    callback(result);
  });
}

// If provided, enabled = 1 or 0 searches for only categories that are enabled or not
// If not provided, returns all categories
var get_category = function(user_id, service, which, callback, enabled) {
  if (enabled !== undefined) {
    q = "SELECT * FROM categories WHERE user_id = ? AND service = ? AND enabled = " + enabled + " ORDER BY id LIMIT ?,1"
  } else {
    q = "SELECT * FROM categories WHERE user_id = ? AND service = ? ORDER BY id LIMIT ?,1"
  }
  connection.query(q, [user_id, service, which-1], function(error, results, fields) {
    if (error) {
      console.log(`Error retrieving category ${which}: ` + error.sqlMessage);
      callback(0);
      return;
    } 
	try {
        result = results[0].name;
	} catch(err) {
		console.log("Error reading DB result for user_id " + user_id);
		result = "";
	}
    callback(result);
  });
}

// Returns the whole list of categories in JSON format
var get_category_list = function(user_id, service, callback, enabled) {
  if (enabled !== undefined) {
    q = "SELECT name FROM categories WHERE user_id = ? AND service = ? AND enabled = " + enabled + " ORDER BY id"
  } else {
    q = "SELECT name FROM categories WHERE user_id = ? AND service = ? ORDER BY id"
  }
  connection.query(q, [user_id, service], function(error, results, fields) {
    if (error) {
      console.log(`Error retrieving category list: ` + error.sqlMessage);
      callback("");
      return;
    } 
	try {
		category_list = []
		results.forEach(function(res) {
			category_list.push(res.name)
		});
		result = JSON.stringify(category_list)
	} catch(err) {
		console.log("Error reading DB result for user_id " + user_id);
		result = "";
	}
    callback(result);
  });
}

module.exports = {save_to_db: save_user, num_categories: num_categories, get_category: get_category, get_category_list: get_category_list};





